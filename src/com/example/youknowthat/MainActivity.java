package com.example.youknowthat;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;



import android.os.Bundle;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class MainActivity extends Activity implements OnItemClickListener {

	String c ="";
	int highS;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		File infile = getBaseContext().getFileStreamPath("province.dat");
		if (infile.exists()) {
			try {
				BufferedReader reader = new BufferedReader( new FileReader (infile));
				
				c = reader.readLine().trim();
				highS =Integer.parseInt(c);
				reader.close();
			} catch (Exception e) {
				//Do nothing
				  
			}
		}
		
		TextView hscore = (TextView)findViewById(R.id.tv1);
		hscore.setText(c);
		
		  
        final Button StartG = (Button) findViewById(R.id.bstart);
        
        // Perform action on click        
        StartG.setOnClickListener(new View.OnClickListener() {
       
        public void onClick(View v) {
      
        // Open Form 2
        
        Intent newActivity = new Intent(MainActivity.this,game.class);
        
        startActivity(newActivity);
     
         
     
        }
     
        });
        
        
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
		
	}



	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}
    
}